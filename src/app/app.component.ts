import {Component, OnInit} from '@angular/core';
import { Item, Cart} from './models';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent  implements OnInit{


  items: Item[] = [];
  cart: Cart = {
    id: 0,
    items: [],
    volume: 0
  };
  constructor(private router: Router, private http: HttpClient) {
    this.getItems();
  }

  ngOnInit(): void {
    this.http.get('http://127.0.0.1:8000').subscribe(res => {
      this.items = res as Item[];
    });
    this.http.post('http://127.0.0.1:8000/create', {}, ).subscribe(res => {
      this.cart = res as Cart;
      console.log(res);
    });
  }

  getList() {
    return this.router.url === '/items' ? this.items : this.cart.items;
  }
  getStatus() {
    return this.router.url === '/items' ? 'add-cart' : 'remove-cart';
  }

  checkURL(url: string) {
    return this.router.url === `/${url}`;
  }
  updateItems() {
    this.http.post(`http://127.0.0.1:8000/update/${this.cart.id}`, this.cart).subscribe(res => {
      console.log(res);
    }, err => {});
  }
  deleteCart() {

    this.http.post(`http://127.0.0.1:8000/delete/${this.cart.id}`, this.cart).subscribe(res => {
      console.log(res);
    }, err => {});
    this.cart.items = [];
  }

  getItems() {
    this.http.get('http://127.0.0.1:8000/').subscribe(
      res => {
        console.log('res' + res.toString());
      }, err => {
        console.log('error' + err);
      });
  }
}
