import { Component, OnInit, Input } from '@angular/core';
import {Item} from '../models';
import {AppComponent} from '../app.component';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {
  @Input()
  item: Item;

  @Input()
  status: 'add-cart' | 'remove-cart';

  constructor(private appComponent: AppComponent) { }

  ngOnInit() {
  }

  addCart() {
    this.appComponent.cart.items.push(this.item);
    this.appComponent.updateItems();
  }

  removeCart() {
    this.appComponent.cart.items.splice(this.appComponent.cart.items.indexOf(this.item), 1);
    this.appComponent.updateItems();
  }

}
