
export interface Item {
  id: number;
  name: string;
  description: string;
  value: number;
  volume: number;
}


export interface Cart {
  items: Item[];
  id: number;
  volume:  number;
}
